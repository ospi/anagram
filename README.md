The AnagramFinder app
==============

Description
--------------
This app finds every word that has one or more anagrams in the list, and list them together. Only one-word anagrams are found. Anagrams are words or expressions containing the same characters put together to form a new word or an expression (http://no.wikipedia.org/wiki/Anagram).

The file eventyr.txt (UTF-8 encoded) found in the resource folder contains a list of words. One word per line.

Example
--------------
The original sorting of the words is not maintained. Every line in the result contains the words that are anagrams of eachother. For instance, eventyr.txt generates the following anagrams:

    rokk krok 
    steinhelle hellestein 
    glinste glinset 
    kristent kristnet 
    bry byr 
    skinte kisten 
    sen ens 
    suten stuen 
    engang gangen 
    ende nede 
    rette etter 
    lysten lysnet 
    ta at 
    bra bar 
    løst støl 
    torde ordet 
    tolv lovt 
    dra rad 
    enden denne 
    rod ord dro 
    rom mor 
    ristet sitter 
    rå år 
    niste stien 
    søsteren søstrene 
    truet turte 
    den ned 
    med dem 
    navn vann 


How to run
--------------
Use the main-method in the Main-class.
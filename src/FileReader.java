


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class FileReader {

	//This is a utility class.
	private FileReader() {throw new UnsupportedOperationException();}
	
	private final static Charset ENCODING = StandardCharsets.UTF_8;
	
	public static List<String> readWordsFromFile(String filePath) {
		Path path = Paths.get(filePath);
		
		if (!Files.isRegularFile(path))
			throw new AnagramIOException("File not existing. Could not find file located at: " + path + ".");
		
		try {
			List<String> words = new ArrayList<>();
			for (String word : Files.readAllLines(path, ENCODING)) {
				if (!word.trim().isEmpty())
					words.add(word);
			}
			return words;
		} catch (IOException e) {
			throw new AnagramIOException("Error occurred while reading file located at: " + filePath + ".", e);
		}
	}
	
}

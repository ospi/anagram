import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


public class Main {

	public static void main(String[] args) {
		String filePath = "resources/eventyr.txt";
		List<String> words = FileReader.readWordsFromFile(filePath);
		Map<String, Set<String>> anagramsGroupedByCharacters = AnagramFinder.findAnagramsGroupedByCharacters(words);
		
		for (Iterator<Entry<String, Set<String>>> anagramsIterator = anagramsGroupedByCharacters.entrySet().iterator(); anagramsIterator.hasNext(); ) {
			Set<String> anagrams = anagramsIterator.next().getValue();
			for (String anagram : anagrams) {
				System.out.print(anagram + " ");
			}
			System.out.println();
		}
	}

}

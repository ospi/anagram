
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class AnagramFinder {
	
	//This is a utility class.
	private AnagramFinder() {throw new UnsupportedOperationException();}
	
	public static Map<String, Set<String>> findAnagramsGroupedByCharacters(List<String> words) {
		Map<String, Set<String>> wordsGroupedByCharacters = findWordsGroupedByCharacters(words);
		removeNonAnagramWords(wordsGroupedByCharacters);
		return wordsGroupedByCharacters;
	}
	
	private static Map<String, Set<String>> findWordsGroupedByCharacters(List<String> words) {
		Map<String, Set<String>> wordsGroupedByCharacters = new HashMap<>();
		
		for (String word : words) {
			char[] chars = word.toLowerCase().toCharArray();
			Arrays.sort(chars);
			String sortedCharsAsString = new String(chars);
			
			if (!wordsGroupedByCharacters.containsKey(sortedCharsAsString))
				wordsGroupedByCharacters.put(sortedCharsAsString, new HashSet<String>());
			
			Set<String> wordsWithSameCharacters = wordsGroupedByCharacters.get(sortedCharsAsString);
			wordsWithSameCharacters.add(word);
		}
		return wordsGroupedByCharacters;
	}
	
	private static void removeNonAnagramWords(Map<String, Set<String>> wordsGroupedByCharacters) {
		for (Iterator<Entry<String, Set<String>>> groupedWordsIterator = wordsGroupedByCharacters.entrySet().iterator(); groupedWordsIterator.hasNext(); ) {
			Set<String> wordsWithSameCharacters = groupedWordsIterator.next().getValue();
			
			//Two or more words must contain the same characters to be anagrams.
			if (wordsWithSameCharacters.size() < 2)
				groupedWordsIterator.remove();
		}
	}

}

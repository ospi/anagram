

public class AnagramIOException extends RuntimeException {

	private static final long serialVersionUID = 5810540614854195307L;

	public AnagramIOException(String message) {
		super(message);
	}
	
	public AnagramIOException(String message, Throwable e) {
		super(message, e);
	}
	
}

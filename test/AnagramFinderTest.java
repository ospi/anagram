
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class AnagramFinderTest {
	
	@Test
	public void shouldFindAnagrams() {
		List<String> words = Arrays.asList("at", "bar", "bra", "bry", "byr", "dem", "den", "dro", "med", "ned", "ord", "rod", "ta");
		Map<String, Set<String>> actualAnagrams = AnagramFinder.findAnagramsGroupedByCharacters(words);
		
		Map<String, Set<String>> expectedAnagramsGroupedByCharacters = new HashMap<>();
		expectedAnagramsGroupedByCharacters.put("at", new HashSet<>(Arrays.asList("at", "ta")));
		expectedAnagramsGroupedByCharacters.put("abr", new HashSet<>(Arrays.asList("bar", "bra")));
		expectedAnagramsGroupedByCharacters.put("bry", new HashSet<>(Arrays.asList("bry", "byr")));
		expectedAnagramsGroupedByCharacters.put("dem", new HashSet<>(Arrays.asList("dem", "med")));
		expectedAnagramsGroupedByCharacters.put("den", new HashSet<>(Arrays.asList("den", "ned")));
		expectedAnagramsGroupedByCharacters.put("dor", new HashSet<>(Arrays.asList("dro", "ord", "rod")));
		
		assertEquals(expectedAnagramsGroupedByCharacters, actualAnagrams);
	}
	
	@Test
	public void shouldFindNoAnagrams() {
		List<String> words = Arrays.asList("at", "bar", "byr", "den", "dro", "med");
		Map<String, Set<String>> actualAnagrams = AnagramFinder.findAnagramsGroupedByCharacters(words);
		Map<String, Set<String>> expectedAnagramsGroupedByCharacters = new HashMap<>();
		
		assertEquals(expectedAnagramsGroupedByCharacters, actualAnagrams);
	}

	@Test
	public void shouldFindNoAnagramsWhenThereAreNoWords() {
		List<String> words = Arrays.asList();
		Map<String, Set<String>> actualAnagrams = AnagramFinder.findAnagramsGroupedByCharacters(words);
		Map<String, Set<String>> expectedAnagramsGroupedByCharacters = new HashMap<>();
		
		assertEquals(expectedAnagramsGroupedByCharacters, actualAnagrams);
	}
	
	@Test
	public void shouldNotBeCaseSensitiveFindingAnagrams() {
		List<String> words = Arrays.asList("at", "bar", "bra", "TA");
		Map<String, Set<String>> actualAnagrams = AnagramFinder.findAnagramsGroupedByCharacters(words);
		Map<String, Set<String>> expectedAnagramsGroupedByCharacters = new HashMap<>();
		expectedAnagramsGroupedByCharacters.put("at", new HashSet<>(Arrays.asList("at", "TA")));
		expectedAnagramsGroupedByCharacters.put("abr", new HashSet<>(Arrays.asList("bar", "bra")));
		
		assertEquals(expectedAnagramsGroupedByCharacters, actualAnagrams);
	}
	
}

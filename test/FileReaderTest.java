
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;


public class FileReaderTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	
	@Test
	public void shouldReadWordsFromFile() {
		String filePath = "test/resources/testAnagrams.txt";
		List<String> actualWords = FileReader.readWordsFromFile(filePath);
		
		assertEquals(6, actualWords.size());
		assertEquals("at", actualWords.get(0));
		assertEquals("bar", actualWords.get(1));
		assertEquals("bra", actualWords.get(2));
		assertEquals("bjørn", actualWords.get(3));
		assertEquals("bjørnen", actualWords.get(4));
		assertEquals("ta", actualWords.get(5));
	}

	@Test
	public void shouldFindNoWordsReadingEmptyFile() throws IOException {
		File temporaryFile = temporaryFolder.newFile("empty.txt");
		List<String> actualWords = FileReader.readWordsFromFile(temporaryFile.getAbsolutePath());
		assertTrue(actualWords.isEmpty());
	}

	@Test
	public void shouldThrowExceptionWhenFilePathIsAnEmptyFolder() {
		expectedException.expect(AnagramIOException.class);
		expectedException.expectMessage("File not existing. Could not find file located at:");
		String filePath = "test/resources/";
		FileReader.readWordsFromFile(filePath);	
	}
	
	@Test
	public void shouldThrowExceptionWhenFileNotExisting() {
		expectedException.expect(AnagramIOException.class);
		expectedException.expectMessage("File not existing. Could not find file located at:");
		String filePath = "test/resources/not_existing.txt";
		FileReader.readWordsFromFile(filePath);	
	}
	
	@Test
	public void shouldThrowExceptionWhenNoPathExists() {
		expectedException.expect(AnagramIOException.class);
		expectedException.expectMessage("File not existing. Could not find file located at:");
		String filePath = "";
		FileReader.readWordsFromFile(filePath);	
	}
	
}
